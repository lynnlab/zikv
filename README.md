# README #

This repo contains files for the RNA-Seq of ZIKV infected human cells. Please see associated manuscript for details. 


Meta_data.xlsx - Per sample associated meta data
FeatureCounts_matrix.txt - Per gene count matrix generated from FeatureCounts
Kraken_Assignments.xlsx - Relative abundance of organisms per sample as estimated by Kraken2
RNA-Seq analysis.R - R code for analysis presented in manuscript
